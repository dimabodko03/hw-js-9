const arrConst = ["hello", "world", "Kyiv", "Kharkiv", "Odessa", "Lviv"];

function listArr(arr) {
    const list = document.createElement('ul');

    for (const item of arr) {
        const listItem = document.createElement('li');
        listItem.textContent = item;
        list.appendChild(listItem);
    }

    document.body.appendChild(list);
}

listArr(arrConst);
